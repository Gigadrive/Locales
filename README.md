# Gigadrive Internationalization
[![Weblate](http://translate.gigadrivegroup.com/weblate/widgets/gigadrive/-/svg-badge.svg)](http://translate.gigadrivegroup.com/weblate/engage/gigadrive/?utm_source=widget)

## Contribute
Please visit our [Weblate page](https://gigadrivegroup.com/translate) to contribute. To become a proofreader, please email [support@gigadrivegroup.com](mailto:support@gigadrivegroup.com).